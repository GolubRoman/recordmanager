//
//  UILabelWithInsets.swift
//  UdacityCourseProject
//
//  Created by Roman Holub on 9/20/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import UIKit

class UILabelWithInsets: UILabel {
    
    override func draw(_ rect: CGRect) {
        let inset = UIEdgeInsets(top: 40, left: 20, bottom: 40, right: 20)
        super.drawText(in: rect.inset(by: inset))
    }
}
