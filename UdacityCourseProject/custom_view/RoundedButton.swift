//
//  ShadowButton.swift
//  UdacityCourseProject
//
//  Created by Roman Holub on 9/9/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {
    
    private var fillColor: UIColor = .white
    
    override func draw(_ rect: CGRect) {
        roundCorners()
        adjustFont()
    }
    
    private func roundCorners() {
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.bottomLeft , .topLeft, .bottomRight, .topRight], cornerRadii: CGSize(width: 15, height: 15))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        layer.mask = maskLayer
    }
    
    private func adjustFont() {
        titleLabel?.numberOfLines = 1
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.lineBreakMode = NSLineBreakMode.byClipping
    }
    
}
