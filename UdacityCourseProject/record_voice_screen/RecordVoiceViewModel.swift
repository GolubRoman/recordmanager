//
//  RecordVoiceViewModel.swift
//  UdacityCourseProject
//
//  Created by Roman Holub on 9/26/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import AVFoundation
import RxSwift

class RecordVoiceViewModel: NSObject, AVAudioRecorderDelegate {
    
    private var audioRecorder: AVAudioRecorder!
    private var isRecording = false
    private var recordAudioEmitter = PublishSubject<RecordAudioEvent>()
    
    override init() { super.init() }
    
    func setIsRecording(isRecording: Bool) {
        self.isRecording = isRecording
    }
    
    func isNowRecording() -> Single<Bool> {
        return Single.just(isRecording)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
    }
    
    func recordAudio() {
        let dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask, true)[0] as String
        let recordingName = "recordedVoice.wav"
        let pathArray = [dirPath, recordingName]
        let filePath = URL(string: pathArray.joined(separator: "/"))
        
        let session = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
        
        audioRecorder = try! AVAudioRecorder(url: filePath!, settings: [:])
        audioRecorder.delegate = self
        audioRecorder.isMeteringEnabled = true
        audioRecorder.prepareToRecord()
        audioRecorder.record()
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        recordAudioEmitter.onNext(RecordAudioEvent(audioUrl: recorder.url, successFlag: flag))
    }
    
    func observeRecordAudioEvents() -> Observable<RecordAudioEvent> {
        return recordAudioEmitter
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
    }
    
    func stopRecordingAudio() {
        if audioRecorder.isRecording {
            audioRecorder.stop()
        }
    }
    
}
