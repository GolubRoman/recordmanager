//
//  RecordAudioEvent.swift
//  UdacityCourseProject
//
//  Created by Roman Holub on 9/26/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation

class RecordAudioEvent {
    var audioUrl: URL? = nil
    var successFlag: Bool = false
    
    convenience init(audioUrl: URL?, successFlag: Bool) {
        self.init()
        self.audioUrl = audioUrl
        self.successFlag = successFlag
    }
    
}
