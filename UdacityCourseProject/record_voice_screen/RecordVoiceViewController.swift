//
//  ViewController.swift
//  UdacityCourseProject
//
//  Created by Roman Holub on 9/4/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

class RecordVoiceViewController: UIViewController {

    private var viewModel = RecordVoiceViewModel()
    private let disposeBag = DisposeBag()
    @IBOutlet weak var recordVoiceButton: RoundedButton!
    
    @IBAction func onRecordPress(_ sender: UIButton) {
        viewModel.isNowRecording().subscribe(onSuccess: { isRecording in
            self.processRecordButtonClick(isRecording: isRecording, button: sender)
        }).disposed(by: disposeBag)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.insetsLayoutMarginsFromSafeArea = false
        recordVoiceButton.setBackgroundImage(UIImage(named: "Record"), for: UIControl.State.normal)
        observeRecordAudioEvents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSecondViewController" {
            let destinationController = segue.destination as! SoundEffectsViewController
            destinationController.setFilePath(filePath: sender as? URL)
        }
    }
    
    private func processRecordButtonClick(isRecording: Bool, button: UIButton) {
        if !isRecording {
            button.setTitle("Stop recording", for: .normal)
            viewModel.recordAudio()
        } else {
            button.setTitle("Record voice", for: .normal)
            viewModel.stopRecordingAudio()
        }
        viewModel.setIsRecording(isRecording: !isRecording)
    }
    
    private func observeRecordAudioEvents() {
        viewModel.observeRecordAudioEvents()
            .subscribe(onNext: { audioEvent in
                self.processAudioEvent(audioEvent: audioEvent)
            }).disposed(by: disposeBag)
    }
    
    private func processAudioEvent(audioEvent: RecordAudioEvent) {
        if audioEvent.successFlag {
            performSegue(withIdentifier: "goToSecondViewController", sender: audioEvent.audioUrl)
        } else {
            self.showRecordingFailedAlert()
        }
    }
    
    private func showRecordingFailedAlert() {
        let alert = UIAlertController(title: "Voice Recording", message: "Voice recording failed, please,try again", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }

}

