//
//  SecondViewController.swift
//  UdacityCourseProject
//
//  Created by Roman Holub on 9/17/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import UIKit
import AVFoundation
import RxSwift
import RxCocoa
import RxGesture

@objcMembers class SoundEffectsViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var viewModel = SoundEffectsViewModel()
    var buttonsEnabled = true
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupAudio()
        initCollectionView()
    }
    
    func setFilePath(filePath: URL!) {
        viewModel.setFilePath(filePath: filePath)
    }
    
    private func initCollectionView() {
        collectionView.dataSource = nil
        setCollectionViewCellSize()
        processSoundEffectsList()
        processCollectionViewCellTap()
    }
    
    private func processSoundEffectsList() {
        viewModel.observeSoundEffects().asObservable().bind(to: collectionView.rx.items(cellIdentifier: "soundEffectViewCell", cellType: SoundEffectViewCell.self)){ row, data, cell in
            cell.data = data
            }.disposed(by: disposeBag)
    }
    
    private func processCollectionViewCellTap() {
        collectionView.rx.modelSelected(SoundEffect.self).subscribe(onNext: {soundEffect in
            self.viewModel.playSoundForSoundEffect(soundEffect: soundEffect)
        }).disposed(by: disposeBag)
    }
    
    private func setCollectionViewCellSize() {
        let width = (collectionView.frame.size.width) / 2
        let height = (collectionView.frame.size.height) / 3
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: height)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
    }
    
}
