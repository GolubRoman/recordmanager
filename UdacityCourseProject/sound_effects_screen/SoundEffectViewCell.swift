//
//  SoundEffectViewCell.swift
//  UdacityCourseProject
//
//  Created by Roman Holub on 9/19/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import UIKit

class SoundEffectViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var data: SoundEffect? {
        didSet {
            guard let data = data else {
                return
            }
            initCell(soundEffect: data)
        }
    }
    
    func initCell(soundEffect: SoundEffect) {
        backgroundImageView.image = UIImage(named: soundEffect.photoUrl)
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.clipsToBounds = true
        
        titleLabel.text = soundEffect.title
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.2
    }
}
