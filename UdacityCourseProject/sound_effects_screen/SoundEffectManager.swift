//
//  SoundEffectManager.swift
//  UdacityCourseProject
//
//  Created by Roman Holub on 9/26/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import AVFoundation

class SoundEffectManager: NSObject, AVAudioPlayerDelegate {
    
    private var recordedAudioURL: URL!
    private var audioFile: AVAudioFile!
    private var audioEngine: AVAudioEngine!
    private var audioPlayerNode: AVAudioPlayerNode!
    private var stopTimer: Timer!
    private var attachedNodes: Array<AVAudioNode> = []
    
    func setupAudio() {
        do { audioFile = try AVAudioFile(forReading: recordedAudioURL) }
        catch { print(String(describing: error)) }
    }
    
    func setFilePath(filePath: URL!) {
        self.recordedAudioURL = filePath
    }
    
    private func attachRatePitchNodeToAudio(audioEngine: AVAudioEngine, rate: Float?, pitch: Float?) {
        let ratePitchNode = AVAudioUnitTimePitch()
        if let rate = rate { ratePitchNode.rate = rate }
        if let pitch = pitch { ratePitchNode.pitch = pitch }
        audioEngine.attach(ratePitchNode)
        attachedNodes.append(ratePitchNode)
    }
    
    private func attachEchoNodeToAudio(audioEngine: AVAudioEngine, echo: Bool) {
        let echoNode = AVAudioUnitDistortion()
        echoNode.loadFactoryPreset(.multiEcho1)
        audioEngine.attach(echoNode)
        if echo { attachedNodes.append(echoNode) }
    }
    
    private func attachReverbNodeToAudio(audioEngine: AVAudioEngine, reverb: Bool) {
        let reverbNode = AVAudioUnitReverb()
        reverbNode.loadFactoryPreset(.cathedral)
        reverbNode.wetDryMix = 50
        audioEngine.attach(reverbNode)
        if reverb { attachedNodes.append(reverbNode) }
    }
    
    private func startEngine() -> Bool {
        do { try audioEngine.start() }
        catch {
            print(String(describing: error))
            return false
        }
        return true
    }
    
    private func initInitialAudioState() {
        audioEngine = AVAudioEngine()
        audioPlayerNode = AVAudioPlayerNode()
        audioEngine.attach(audioPlayerNode)
        attachedNodes.removeAll()
    }
    
    func playSound(rate: Float? = nil, pitch: Float? = nil, echo: Bool = false, reverb: Bool = false) {
        initInitialAudioState()
        
        attachedNodes.append(audioPlayerNode)
        attachRatePitchNodeToAudio(audioEngine: audioEngine, rate: rate, pitch: pitch)
        attachEchoNodeToAudio(audioEngine: audioEngine, echo: echo)
        attachReverbNodeToAudio(audioEngine: audioEngine, reverb: reverb)
        attachedNodes.append(audioEngine.outputNode)
        connectAudioNodes(nodes: attachedNodes)
        
        audioPlayerNode.stop()
        audioPlayerNode.scheduleFile(audioFile, at: nil) {
            self.scheduleTimerForDelay(delay: self.calculateDelayForAudio(rate: rate))
        }
        
        if !startEngine() { return }
        
        audioPlayerNode.play()
    }
    
    private func calculateDelayForAudio(rate: Float?) -> Double {
        var delayInSeconds: Double = 0
    
        if let lastRenderTime = self.audioPlayerNode.lastRenderTime, let playerTime = self.audioPlayerNode.playerTime(forNodeTime: lastRenderTime) {
            delayInSeconds = Double(self.audioFile.length - playerTime.sampleTime) / Double(self.audioFile.processingFormat.sampleRate)
            if let rate = rate { delayInSeconds /= Double(rate) }
        }
        return delayInSeconds
    }
    
    private func scheduleTimerForDelay(delay: Double) {
        self.stopTimer = Timer(timeInterval: delay, target: self, selector: #selector(self.stopAudio), userInfo: nil, repeats: false)
        RunLoop.main.add(self.stopTimer!, forMode: RunLoop.Mode.default)
    }
    
    @objc func stopAudio() {
        if let audioPlayerNode = audioPlayerNode { audioPlayerNode.stop() }
        if let stopTimer = stopTimer { stopTimer.invalidate() }
        if let audioEngine = audioEngine {
            audioEngine.stop()
            audioEngine.reset()
        }
    }
    
    private func connectAudioNodes(nodes: Array<AVAudioNode>) {
        for x in 0..<nodes.count-1 {
            audioEngine.connect(nodes[x], to: nodes[x+1], format: audioFile.processingFormat)
        }
    }
}
