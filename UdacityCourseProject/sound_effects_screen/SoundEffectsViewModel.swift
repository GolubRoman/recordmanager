//
//  SoundEffectsViewModel.swift
//  UdacityCourseProject
//
//  Created by Roman Holub on 9/25/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import AVFoundation
import RxSwift

class SoundEffectsViewModel: NSObject {
    
    private var soundEffects: Array<SoundEffect> = []
    private var soundEffectManager = SoundEffectManager()
    
    override init() {
        super.init()
        soundEffects = []
        initDefaultState()
    }
    
    func setupAudio() { self.soundEffectManager.setupAudio() }
    
    func setFilePath(filePath: URL!) { self.soundEffectManager.setFilePath(filePath: filePath) }
    
    private func initDefaultState() {
        SoundEffectType.allCases.forEach {
                soundEffects.append(SoundEffect(id: $0.photoUrl, photoUrl: $0.photoUrl, title: $0.title, type: $0.self))
            }
    }
    
    func observeSoundEffects() -> Maybe<Array<SoundEffect>> {
        return Maybe.create { maybe in
            if self.soundEffects.count > 0 {
                maybe(.success(self.soundEffects))
            } else {
                maybe(.completed)
            }
            return Disposables.create()
        }.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
    }
    
    func playSoundForSoundEffect(soundEffect: SoundEffect) {
        switch(soundEffect.type) {
            case .slow: soundEffectManager.playSound(rate: 0.5)
            case .rapid: soundEffectManager.playSound(rate: 1.5)
            case .squirrel: soundEffectManager.playSound(pitch: 1000)
            case .darth: soundEffectManager.playSound(pitch: -1000)
            case .owl: soundEffectManager.playSound(echo: true)
            case .wind: soundEffectManager.playSound(reverb: true)
            default: soundEffectManager.playSound(rate: 1)
        }
    }
    
}
