//
//  SoundEffect.swift
//  UdacityCourseProject
//
//  Created by Roman Holub on 9/24/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation

class SoundEffect {
    var id: String = ""
    var photoUrl: String = ""
    var title: String = ""
    var type: SoundEffectType = SoundEffectType.standard
    
    convenience init(id: String, photoUrl: String, title: String, type: SoundEffectType) {
        self.init()
        self.id = id
        self.photoUrl = photoUrl
        self.title = title
        self.type = type
    }

}

public enum SoundEffectType: CaseIterable { case slow, rapid, squirrel, darth, owl, wind, standard
    var photoUrl: String {
        switch self {
        case .slow: return "slow"
        case .rapid: return "rapid"
        case .squirrel: return "squirrel"
        case .darth: return "darth"
        case .owl: return "owl"
        case .wind: return "wind"
        case .standard: return "standard"
        }
    }
    
    var title: String {
        switch self {
        case .slow: return "Make your voice slower"
        case .rapid: return "Rapid your voice"
        case .squirrel: return "Speak like a squirrel"
        case .darth: return "Want to be Darth Vader"
        case .owl: return "Speak like you are owl"
        case .wind: return "Wind voice"
        case .standard: return "standard"
        }
    }
}
